Raffle
======

HTML and Javascript Raffle System

Demo renders better on PC for Now.
**Demo <a href='https://www.igbonekwukelechi.com/raffle/'>here</a>.**

How To Use
-----------

In js/raffle.js contains all javascript code used in the raffle. 

This raffle system allows you to initialize the three colors of tickets to be used in the raffle draw by entering ranges which the tickets will span.

Assuming for red tickets, the range entered is "1 to 10", A string will be created as such "1,2,3,4,5,6,7,8,9,10"
which will be converted to an array of JSON objects. Each JSON object contains a name, and can optionally contain points. A default of 1 point is given to names without point.
An example JSON is shown

	var imported = [
		{
			name: 1,
			points: 3
		},{
			name: 2,
			points: 5
		},{
			name: 3  //no points defaults to 1
		},{
			name: 4,
			points: 1
		}
	];

The data is then used to create tickets which will be used ofr the raffle.<br>
<b>Check it out and give your feedback. </b>