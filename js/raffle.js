/**
 * Raffle
 * 2012
 * https://github.com/stringham/raffle
 * Copyright Ryan Stringham
 */

let inProgress = false;
let size = 60;

let redData = "";
let blueData = "";
let greenData = "";

let activeColor = "";
let activeColor2 = "";
let activeColor3 = "";

let imported;
let imported2;
let imported3;

let colors = ["#FF0000","#008000","#0000FF","#FFFF00"];

function map(a, f){
	for(let i=0; i<a.length; i++){
		f(a[i], i);
	}
}

function getColour(a){
    let val;
    switch (a) {
        case "red":
            val = 0;
            break;
        case "green":
            val = 1;
            break;
        case "blue":
            val = 2;
            break;
    }
    return val;
}

function processRange(){
	redData = formString($("#fromRed").val(),$("#toRed").val());
	greenData = formString($("#fromGreen").val(),$("#toGreen").val());
	blueData = formString($("#fromBlue").val(),$("#toBlue").val());

    $(".enter-names").hide();
    $("#toShuffle").removeClass("d-none");
    $("#testShuffle").removeClass("d-none");

    //console.log( redData + "blue");
}

function formString(x,y){
    x = parseInt(x, 10);
    y = parseInt(y, 10);
	let theData = "";
	let i;
    for (i = x; i <= y; i++){
        theData = theData + i+ "\n";
    }
    return theData;
}

function getNames(a) {
	return eval(a+"Data").split('\n').filter(function(name) {
		return name.trim();
	});
}

/**
* process functions creates the array of data and calls the respective function
* makeTicketsWithPoints() points to create the raffle tickets.
* the number of parameters specifies the number of colors being combined.
* */
function process(a){

	imported = [];
	map(getNames(a), function(name){
		imported.push({'name':name});
	});
	activeColor = a;
	$('#toShuffle').hide(500, function(){
		//console.log(imported);
		makeTicketsWithPoints();
	});
}

function process2(a,b){

    imported = [];
    imported2 = [];

    map(getNames(a), function(name){
        imported.push({'name':name});
    });
    map(getNames(b), function(name){
        imported2.push({'name':name});
    });
    activeColor = a;
    activeColor2 = b;
    $('#toShuffle').hide(500, function(){
        //console.log(imported);
        makeTicketsWithPoints2();
    });
}

function process3(a,b,c){

    imported = [];
    imported2 = [];
    imported3 = [];

    map(getNames(a), function(name){
        imported.push({'name':name});
    });
    map(getNames(b), function(name){
        imported2.push({'name':name});
    });
    map(getNames(c), function(name){
        imported3.push({'name':name});
    });
    activeColor = a;
    activeColor2 = b;
    activeColor3 = c;

    $('#toShuffle').hide(500, function(){
        //console.log(imported);
        makeTicketsWithPoints3();
    });
}

$(document).on("keypress", function (e) {
	console.log(e.which);
	if(e.which === 115 || e.which === 85 ){//s
        pickName();
	}else if(e.which === 114 || e.which === 82){//r
		let r = "red";
		process(r);
	}else if(e.which === 103 || e.which === 71){//g
        let g = "green";
        process(g);
    }else if(e.which === 98 || e.which === 66){//b
        let b = "blue";
        process(b);
    }else if(e.which === 121 || e.which === 89){//y
        process2('red','green');
    }else if(e.which === 109 || e.which === 77){//m
        process2('red','blue');
    }else if(e.which === 107 || e.which === 75){//k
        process2('green','blue');
    }else if(e.which === 97 || e.which === 65){//a
        process3('red','green','blue');
    }
});

$(document).ready(function(){
	if(imported && imported.length > 0) {
		$('.enter-names').hide();

		makeTicketsWithPoints();
	}

	/*$('.name-text-field').on('input', function() {
		$('#participant-number').text(getNames().length || '');
	});*/


});
let ticketNames;
let ticketPoints;

function elementInViewport(el) {
  let top = el.offsetTop;
  let left = el.offsetLeft;
  let width = el.offsetWidth;
  let height = el.offsetHeight;

  while(el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }

  return (
    top >= window.pageYOffset &&
    left >= window.pageXOffset &&
    (top + height) <= (window.pageYOffset + window.innerHeight) &&
    (left + width) <= (window.pageXOffset + window.innerWidth)
  );
}

function Ticket(name, points){
    this.name = name;
    if(typeof(points) == "number")
        this.points = points;
    else
        this.points = 1;
    this.dom = $("<div class='ticket'>").text(name);
    this.fixPosition = function(){
        let me = this;
        this.dom.css({
            'position':'absolute',
            'top': me.dom.offset().top,
            'left': me.dom.offset().left,
            'background': colors[getColour(activeColor)]
        });
    };
    this.decrement = function(length, callback){
        let me = this;
        this.points--;
        if(this.points == 0){
            let directions = ['up', 'down', 'left', 'right'];
            this.dom.css({'background-color':colors[3]}).hide('drop', {direction:directions[length%directions.length]}, length <= 3 ? 750 : 3000/length, function(){
                callback();
            });
            $('#participant-number').text(length - 1 + '/' + tickets.length);
        }
        else{
            this.dom.css({
                'background-color':colors.length > me.points ? colors[me.points] : "rgb(" + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + ")"
            })
            setTimeout(function() {
                callback();
            }, length == 2 ? 1000 : 3000/length);
        }
    }
}

function Ticket2(name, points){
    this.name = name;
    if(typeof(points) == "number")
        this.points = points;
    else
        this.points = 1;
    this.dom = $("<div class='ticket'>").text(name);
    this.fixPosition = function(){
        let me = this;
        this.dom.css({
            'position':'absolute',
            'top': me.dom.offset().top,
            'left': me.dom.offset().left,
            'background': colors[getColour(activeColor2)]
        });
    };
    this.decrement = function(length, callback){
        let me = this;
        this.points--;
        if(this.points == 0){
            let directions = ['up', 'down', 'left', 'right'];
            this.dom.css({'background-color':colors[3]}).hide('drop', {direction:directions[length%directions.length]}, length <= 3 ? 750 : 3000/length, function(){
                callback();
            });
            $('#participant-number').text(length - 1 + '/' + tickets.length);
        }
        else{
            this.dom.css({
                'background-color':colors.length > me.points ? colors[me.points] : "rgb(" + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + ")"
            })
            setTimeout(function() {
                callback();
            }, length == 2 ? 1000 : 3000/length);
        }
    }
}

function Ticket3(name, points){
    this.name = name;
    if(typeof(points) == "number")
        this.points = points;
    else
        this.points = 1;
    this.dom = $("<div class='ticket'>").text(name);
    this.fixPosition = function(){
        let me = this;
        this.dom.css({
            'position':'absolute',
            'top': me.dom.offset().top,
            'left': me.dom.offset().left,
            'background': colors[getColour(activeColor3)]
        });
    };
    this.decrement = function(length, callback){
        let me = this;
        this.points--;
        if(this.points == 0){
            let directions = ['up', 'down', 'left', 'right'];
            this.dom.css({'background-color':colors[3]}).hide('drop', {direction:directions[length%directions.length]}, length <= 3 ? 750 : 3000/length, function(){
                callback();
            });
            $('#participant-number').text(length - 1 + '/' + tickets.length);
        }
        else{
            this.dom.css({
                'background-color':colors.length > me.points ? colors[me.points] : "rgb(" + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + ")"
            })
            setTimeout(function() {
                callback();
            }, length == 2 ? 1000 : 3000/length);
        }
    }
}

let tickets = [];

let removeDuplicateNames = function(data){
	let seen = {};
	return data.filter(function(d){
		if(seen[d.name.toLowerCase()]){
			return false;
		}
		seen[d.name.toLowerCase()] = true;
		return true;
	});
};

let makeTicketsWithPoints = function(){
	tickets = [];
	$('.ticket').remove();
	map(removeDuplicateNames(imported), function(tdata){
		var t = new Ticket(tdata.name, tdata.points);
		if(t.points > 0)
			t.dom.appendTo($('body'));
		tickets.push(t);
	});
	tickets.reverse();
	size = 20;
	$('.ticket').css('font-size', size + 'px');
	while(!elementInViewport(tickets[0].dom.get(0)) && size > 10){
		size--;
		$('.ticket').css('font-size', size + 'px');
	}

	$('#participant-number').css('width', '').text(tickets.length);
	setTimeout(function() {
		map(tickets, function(ticket){
			ticket.fixPosition();
		});
		$('body').unbind('click').click(function(){
			var width = $('#participant-number').text(tickets.length + '/' + tickets.length).width();
			$('#participant-number').css('width', width + 'px'); //keep position consistent during countdown
			pickName();
		});
	}, 500);
};

let  makeTicketsWithPoints2 = function(){
    tickets = [];
    $('.ticket').remove();
    map(imported, function(tdata){
        var t = new Ticket(tdata.name, tdata.points);
        if(t.points > 0)
            t.dom.appendTo($('body'));
        tickets.push(t);
    });
    map(imported2, function(tdata){
        var t = new Ticket2(tdata.name, tdata.points);
        if(t.points > 0)
            t.dom.appendTo($('body'));
        tickets.push(t);
    });
    tickets.reverse();
    size = 20;
    $('.ticket').css('font-size', size + 'px');
    while(!elementInViewport(tickets[0].dom.get(0)) && size > 10){
        size--;
        $('.ticket').css('font-size', size + 'px');
    }

    $('#participant-number').css('width', '').text(tickets.length);
    setTimeout(function() {
        map(tickets, function(ticket){
            ticket.fixPosition();
        });
        $('body').unbind('click').click(function(){
            var width = $('#participant-number').text(tickets.length + '/' + tickets.length).width();
            $('#participant-number').css('width', width + 'px'); //keep position consistent during countdown
            pickName();
        });
    }, 500);
};

let  makeTicketsWithPoints3 = function(){
    tickets = [];
    $('.ticket').remove();
    map(imported, function(tdata){
        var t = new Ticket(tdata.name, tdata.points);
        if(t.points > 0)
            t.dom.appendTo($('body'));
        tickets.push(t);
    });
    map(imported2, function(tdata){
        var t = new Ticket2(tdata.name, tdata.points);
        if(t.points > 0)
            t.dom.appendTo($('body'));
        tickets.push(t);
    });
    map(imported3, function(tdata){
        var t = new Ticket3(tdata.name, tdata.points);
        if(t.points > 0)
            t.dom.appendTo($('body'));
        tickets.push(t);
    });
    tickets.reverse();
    size = 20;
    $('.ticket').css('font-size', size + 'px');
    while(!elementInViewport(tickets[0].dom.get(0)) && size > 10){
        size--;
        $('.ticket').css('font-size', size + 'px');
    }

    $('#participant-number').css('width', '').text(tickets.length);
    setTimeout(function() {
        map(tickets, function(ticket){
            ticket.fixPosition();
        });
        $('body').unbind('click').click(function(){
            var width = $('#participant-number').text(tickets.length + '/' + tickets.length).width();
            $('#participant-number').css('width', width + 'px'); //keep position consistent during countdown
            pickName();
        });
    }, 500);
};

var getChoices = function(){
	var result = [];
	map(tickets, function(ticket){
		if(ticket.points > 0)
			result.push(ticket)
	});
	return result;
}

$(window).resize(function(){
	if(!inProgress)
		makeTicketsWithPoints(tickets);
});


var pickName = function(){
	inProgress = true;
	$('.ticket').unbind('click');
	$('body').unbind('click');
	
	var choices = getChoices();
	if(choices.length > 1){
		var remove = Math.floor(Math.random()*choices.length);
		choices[remove].decrement(choices.length, function(){
			pickName();
		});
	}
	else{
		choices = $(choices[0].dom);
		var top = choices.css('top');
		var left = choices.css('left');
		var fontsize = choices.css('font-size');
		var width = choices.width();
		choices.click(function(){
			inProgress = false;
			choices.animate({'font-size':fontsize,'top':top,'left':left},'slow');
			$('.ticket').show(500).unbind('click');
			setTimeout(function(){
				makeTicketsWithPoints(ticketNames, ticketPoints);
			}, 700);
		});
		choices.animate({'font-size':3*size +'px','top':(window.innerHeight/5) + 'px','left':(window.innerWidth/2 - width) + 'px'},1500, function(){
			choices.animate({'left':(window.innerWidth/2 - choices.width()/2) + 'px'}, 'slow');
		});
	}
};